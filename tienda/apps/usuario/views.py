from .forms import RegistroForm
from django.views.generic import View,TemplateView, DetailView, UpdateView, DeleteView
from django.shortcuts import redirect, render 
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from .forms import RegistroForm, UserEditForm, TipoUsuarioEditForm
from django.urls import reverse_lazy, reverse

from .models import TipoUsuario
class RegistroView(View):
   form_class = RegistroForm
   template_name = 'usuario/registro.html'

   def get(self, request, *args, **kwargs):
       form = self.form_class()
       return render(request, self.template_name, {'form': form})

   def post(self, request, *args, **kwargs):
       form = self.form_class(request.POST)
       if form.is_valid():
           form.save()
           return redirect('producto:home')  # Suponiendo que tienes una URL de login
       return render(request, self.template_name, {'form': form})
   
class claseIndex(TemplateView):
   template_name = "usuario/index.html"

def vendedor_home(request):
   return render(request, 'usuario/vendedor_home.html')

def cliente_home(request):
   return render(request, 'usuario/cliente_home.html')

def user_login(request):
   if request.method == 'POST':
       form = AuthenticationForm(request, data=request.POST)
       if form.is_valid():
           username = form.cleaned_data.get('username')
           password = form.cleaned_data.get('password')
           user = authenticate(username=username, password=password)
           if user is not None:
               login(request, user)
               tipo_usuario = TipoUsuario.objects.get(usuario=user)
               if tipo_usuario.tipo_usuario == TipoUsuario.VENDEDOR:
                   return redirect('usuario:vendedor_home')  # URL para el homepage del vendedor
               else:
                   return redirect('usuario:cliente_home')  # URL para el homepage del client
   else:
       form = AuthenticationForm()
   return render(request, 'usuario/login.html', {'form': form})

def user_logout(request):
   logout(request)
   return redirect('usuario:index')

class UserDetailViewCliente(LoginRequiredMixin, DetailView):
   model = User
   template_name = 'usuario/perfil_cliente.html'

   def get_object(self):
       return self.request.user

   def get_context_data(self, **kwargs):
       context = super().get_context_data(**kwargs)
       usuario = self.get_object()
       context['usuario'] = usuario
       context['tipo_usuario'] = TipoUsuario.objects.filter(usuario=usuario).first()
       return context
   
class UserDetailViewVendedor(LoginRequiredMixin, DetailView):
   model = User
   template_name = 'usuario/perfil_vendedor.html'

   def get_object(self):
       return self.request.user

   def get_context_data(self, **kwargs):
       context = super().get_context_data(**kwargs)
       usuario = self.get_object()
       context['usuario'] = usuario
       context['tipo_usuario'] = TipoUsuario.objects.filter(usuario=usuario).first()
       return context
   
class UserEditView(LoginRequiredMixin, UpdateView):
   model = User
   form_class = UserEditForm
   template_name = 'usuario/actualizar.html'
   success_url=reverse_lazy('usuario:perfil_cliente')

   def get_object(self):
       return self.request.user

   def get_context_data(self, **kwargs):
       context = super().get_context_data(**kwargs)
       if 'tipo_usuario_form' not in context:
           context['tipo_usuario_form'] = TipoUsuarioEditForm(instance=self.get_tipo_usuario())
       return context
   
   def get_tipo_usuario(self):
       return TipoUsuario.objects.get_or_create(usuario=self.request.user)[0]

   def post(self, request, *args, **kwargs):
       self.object = self.get_object()
       form = self.get_form()
       tipo_usuario_form = TipoUsuarioEditForm(request.POST, instance=self.get_tipo_usuario())

       if form.is_valid() and tipo_usuario_form.is_valid():
           return self.form_valid(form, tipo_usuario_form)
       else:
           return self.form_invalid(form, tipo_usuario_form)

   def form_valid(self, form, tipo_usuario_form):
       self.object = form.save()
       tipo_usuario_form.save()
       return super().form_valid(form)

   def form_invalid(self, form, tipo_usuario_form):
       return self.render_to_response(self.get_context_data(form=form, tipo_usuario_form=tipo_usuario_form))
   
class UserDeleteView(LoginRequiredMixin, DeleteView):
   model = User
   template_name = 'usuario/eliminar.html'

   def get_object(self):
       return self.request.user

   def get_success_url(self):
       return reverse('usuario:index')

   def delete(self, request, *args, **kwargs):
       usuario = self.get_object()
       tipo_usuario = TipoUsuario.objects.filter(usuario=usuario).first()
       tipo_usuario.delete()
       usuario.delete()
       return super().delete(request, *args, **kwargs)

