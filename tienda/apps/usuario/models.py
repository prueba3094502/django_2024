from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class TipoUsuario(models.Model):
    CLIENTE = 'Cliente'
    VENDEDOR = 'Vendedor'
    TIPO_USUARIO_OPCIONES =[
        (CLIENTE, 'Cliente'),
        (VENDEDOR, 'Vendedor'),
    ]

    usuario= models.OneToOneField(User, on_delete=models.CASCADE)
    tipo_usuario = models.CharField(max_length=10, choices = TIPO_USUARIO_OPCIONES, default=CLIENTE)

    def __str__(self):
        return f"{self.usuario.username} ({self.usuario.first_name} {self.usuario.last_name})"