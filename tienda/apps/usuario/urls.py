from django.urls import path
from apps.usuario.views import *

app_name='usuario'

urlpatterns = [
    path('claseIndex/', claseIndex.as_view(), name='index'),
    path('registro/', RegistroView.as_view(), name='registro'),
    path('vendedor/home/', vendedor_home, name='vendedor_home'),
    path('cliente/home/', cliente_home, name='cliente_home'), 
    path('login/', user_login, name='login'),
    path('logout/', user_logout, name='logout'),
    path('cliente/perfil/', UserDetailViewCliente.as_view(), name='perfil_cliente'),
    path('vendedor/perfil/', UserDetailViewVendedor.as_view(), name='perfil_vendedor'),
    path('editar/', UserEditView.as_view(), name='editar_usuario'),
    path('eliminar/', UserDeleteView.as_view(), name='eliminar_usuario'),

]



