from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import TipoUsuario

class RegistroForm(UserCreationForm):
    email = forms.EmailField(required=True)
    tipo_usuario = forms.ChoiceField(choices=TipoUsuario.TIPO_USUARIO_OPCIONES, required=True)

    class Meta:
        model = User
        fields = ("username", 'first_name', 'last_name', "email", "password1", "password2", "tipo_usuario")

    def save(self, commit=True):
        user = super().save(commit=False)
        user.email = self.cleaned_data["email"]
        if commit:
            user.save()
            tipo_usuario = TipoUsuario(usuario=user, tipo_usuario=self.cleaned_data["tipo_usuario"])
            tipo_usuario.save()
        return user
    
class UserEditForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ["username", 'first_name', 'last_name', 'email']  # Añade los campos que necesites

class TipoUsuarioEditForm(forms.ModelForm):
    class Meta:
        model = TipoUsuario
        fields = ['tipo_usuario']


