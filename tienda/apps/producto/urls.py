from django.urls import path
from apps.producto.views import (index)
from apps.producto.views import (fecha)
from apps.producto.views import (edad)
from apps.producto.views import (nombre, eliminar_producto)
from apps.producto.views import (datos, home, agregar_producto, listar_productos, editar_producto, claseIndex)

app_name='producto'

urlpatterns = [
    path('index/', index, name='index'),
    path('fecha/', fecha, name='fecha'),
    path('edad/<int:nacimiento>',edad,name='edad'),
    path('nombre/<str:nombre>',nombre, name='nombre'),
    path('datos/<str:nombre>/<int:nacimiento>' ,datos,name='datos'),
    path('home/',home, name='home'),
    path('home/<str:nombre>/<str:edad>', home, name='home'),
    path('agregarProducto/', agregar_producto, name='agregarProd'),
    path('lista/', listar_productos, name='listProd'),
    path('editar/<int:id>/', editar_producto, name ='editProd'),
    path('eliminar/<int:id>/', eliminar_producto, name='elimProd'),
    path('claseIndex/', claseIndex.as_view(), name='claseIndex')
]
