from django.shortcuts import render
from django.http import HttpResponse
from datetime import datetime
from django.template import loader
from django.shortcuts import redirect
from .forms import ProductoForm
from .models import Producto
from django.views.generic import View,TemplateView

def index(request):
    return HttpResponse("Hola amigos de YouTube")

def fecha(request):
    fecha_actual = datetime.now()
    return HttpResponse (fecha_actual)

def edad(request,nacimiento):
    anio= datetime.now().year
    edad=anio-nacimiento
    return HttpResponse(edad)

def nombre(request,nombre):
    saludo='Bienvenido ' +nombre
    return HttpResponse(saludo)

def datos (request, nombre, nacimiento):
        anio= datetime.now().year
        edad= str(anio-nacimiento)
        #saludo='Hola, ¿Cómo estás? '+nombre+ ' tienes ' +str(edad)+ ' años'
        return redirect("producto:home", nombre, edad) 

#def home(request):
#     template = loader.get_template('producto/home.html')
#     context = {}
#     return HttpResponse(template.render(context, request))

def home(request):
    cliente = {"nombre": "Ana", "apellido": "lopez"}
    productos = ["Paleta", "Chicle", "Gomitas", "Mazapán"]
    fecha = datetime.now()
    tpl = 'producto/home.html'
    contexto =  {'cliente': cliente, 'fecha': fecha, 'productos': productos}
    return render(request, tpl, contexto)

def agregar_producto(request):
    if request.method == 'POST':
        #print(request.user)
        form = ProductoForm(request.POST, request.FILES)
        if form.is_valid():
            producto = form.save(commit=False)
            #producto.vendedor = request.user
            producto.save()
            return redirect('producto:listProd')  # Redirige a la vista de listar productos
    else:
        form = ProductoForm()
    return render(request, 'producto/agregar_producto.html', {'form': form}) 
    
def listar_productos(request):
        productos = Producto.objects.all()  # Solo muestra los productos del vendedor actual
        return render(request, 'producto/listar_producto.html', {'productos': productos}) 
    
def editar_producto(request, id):
    producto = Producto.objects.get(id=id)
    if request.method == 'POST':
        form = ProductoForm(request.POST, request.FILES, instance=producto)
        if form.is_valid():    
            form.save()
            return redirect('producto:listProd')
    else:
        form = ProductoForm(instance=producto)
    return render(request, 'producto/editar_producto.html', {'form': form}) 

def eliminar_producto(request, id):
    producto = Producto.objects.get(id=id)
    if request.method == 'POST':
        producto.delete()
        return redirect('producto:ListProd')
    return render(request, 'producto/eliminar_producto.html', {'producto': producto}) 

class claseIndex(TemplateView):
    template_name = "producto/index.html"