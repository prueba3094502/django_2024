from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Producto (models.Model):
    nombre = models.CharField(max_length=50)
    precio = models.DecimalField(max_digits=7, decimal_places=2)
    descripcion = models.TextField(null=True)
    imagen = models.ImageField(upload_to='imagenes_productos/', blank=True, null=True)
    vendedor = models.ForeignKey(User, on_delete=models.CASCADE, related_name='productos')
    stock = models.IntegerField(default=0)

