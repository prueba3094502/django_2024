from django import forms
from .models import Producto

class ProductoForm(forms.ModelForm):
    class Meta:
        model = Producto
        #fields = ['nombre', 'precio', 'descripcion', 'imagen', 'stock'] #para agregar uno a uno los que necesitemos
        #fields = '__all__' "para agarrar todos"
        #exclude = ['vendedor'] #con ese se excluye solo a vendedor y se muestran todos los demas
        fields = '__all__'
