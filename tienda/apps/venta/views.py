from django.views.generic import DetailView
from django.http import HttpResponse
from django.template.loader import get_template
from xhtml2pdf import pisa
from django.shortcuts import redirect, render 
from apps.venta.models import *
import io
from django.http import JsonResponse
from django.core.serializers import serialize
import json
from django.utils import timezone
from apps.carrito.models import ItemCarrito

class ListVentasPdf(DetailView):
    model=Venta
    def get(self,request,*args,**kwargs):
        venta=self.get_object()
        data={
            'venta':venta
        }
        fecha_venta_str = venta.fecha_venta.strftime("%Y%m%d")  # o el formato de fecha que prefieras
        name_pdf = str(venta.usuario).replace(' ', '') + "_" + str(venta.id).replace(' ', '') + "_" + fecha_venta_str + ".pdf"
        #Obtener datos y renderizar el template
        template=get_template('venta/ventaDetail.html')
        html=template.render(data)
        # Crear el archivo PDF en memoria
        result=io.BytesIO()
        pdf=pisa.CreatePDF(io.StringIO(html),result)
        # Crear la respuesta HTTP
        if not pdf.err:
            pdf=HttpResponse(result.getvalue(),content_type="application/pdf")
            pdf['Content-Disposition'] = 'inline; filename="%s"' % name_pdf
        else:
            pdf=HttpResponse(status=500) 
        return pdf

class ListVentasJson(DetailView):
    model = Venta
    def get(self, request, *args, **kwargs):
        venta = self.get_object()
        # Formato de nombre de archivo similar al PDF
        fecha_venta_str = venta.fecha_venta.strftime("%Y%m%d")
        name_json = "{}_{}_{}.json".format(venta.usuario.username.replace(' ', ''), venta.id, fecha_venta_str)
        # Serializar los datos de la venta
        venta_data = json.loads(serialize('json', [venta]))
        items_data = json.loads(serialize('json', venta.items.all()))
        # Crear un diccionario con los datos serializados
        response_data = {
            'venta': venta_data,
            'items': items_data
        }
        # Convertir el diccionario a JSON
        json_data = json.dumps(response_data, indent=4)
        # Crear la respuesta HTTP
        response = HttpResponse(json_data, content_type='application/json')
        response['Content-Disposition'] = 'attachment; filename="{}"'.format(name_json)
        return response


def compra_exitosa(request):
   return render(request, 'venta/compra_exitosa.html')

def realizar_compra(request):
   items_carrito = ItemCarrito.objects.filter(usuario=request.user)
   venta = Venta.objects.create(usuario=request.user, fecha_venta=timezone.now())
   for item in items_carrito:
       item_venta = ItemVenta.objects.create(
           producto=item.producto,
           cantidad=item.cantidad,
           precio=item.producto.precio)
       venta.items.add(item_venta)
       item.producto.save()
       item.delete()
   return redirect('venta:compra_exitosa')

def ventas_vendedor(request):
   ventas = Venta.objects.filter(items__producto__vendedor=request.user).distinct().prefetch_related('items')
   return render(request, 'venta/ventas_vendedor.html', {'ventas': ventas})

def compras_cliente(request):
   ventas = Venta.objects.filter(usuario=request.user).prefetch_related('items')
   return render(request, 'venta/compras_cliente.html', {'ventas': ventas})
