from django.contrib import admin

from apps.venta.models import Venta, ItemVenta
# Register your models here.

class ItemVentaAdmin(admin.ModelAdmin):
    list_display = ('producto', 'cantidad')
    search_fields = ('precio',)
    list_filter=('cantidad',)
# Register your models here.
admin.site.register(ItemVenta, ItemVentaAdmin) 




