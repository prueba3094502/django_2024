from django.urls import path
from apps.venta.views import *

app_name='venta'

urlpatterns = [
    path('compra/exitosa/', compra_exitosa, name='compra_exitosa'),
    path('realizar_compra/', realizar_compra, name='realizar_compra'),
    path('ventas/vendedor/', ventas_vendedor, name='ventas_vendedor'),
    path('compras/cliente/', compras_cliente, name='compras_cliente'),
    path('ventasPDF/<int:pk>', ListVentasPdf.as_view(),name='ventasPDF'), 
    path('vanetasJSON/<int:pk>',ListVentasJson.as_view(),name='ventasJSON'),
    
]