from django.db import models
from apps.producto.models import Producto
from django.contrib.auth.models import User

# Create your models here.
class ItemVenta(models.Model):
    producto = models.ForeignKey(Producto, on_delete=models.CASCADE)
    cantidad = models.IntegerField()
    precio = models.DecimalField(max_digits = 6, decimal_places=2)

    def __str__(self):
       return f"{self.producto.nombre} x{self.cantidad}"
    
    class Meta:
       verbose_name = "item_venta"
       verbose_name_plural = "items_ventas"
       ordering=['precio']
       unique_together=('producto','precio') 

class Venta(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    items = models.ManyToManyField(ItemVenta)
    fecha_venta = models.DateTimeField(auto_now_add = True)  

 