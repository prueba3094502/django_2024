from django.urls import path
from apps.carrito.views import *

app_name='carrito'

urlpatterns = [
    path('agregar/<int:id>/', agregar_carrito, name='agregar_carrito'),
    path('', ver_carrito, name='ver_carrito'),
    path('eliminar/<int:id>/', eliminar_producto_carrito, name='eliminar_producto_carrito'),
    path('incrementar/<int:id>/', incrementar_cantidad_producto_carrito, name='incrementar_cantidad_producto_carrito'),
    path('decrementar/<int:id>/', decrementar_cantidad_producto_carrito, name='decrementar_cantidad_producto_carrito'),
]
