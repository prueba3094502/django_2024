from django.db import models
from django.contrib.auth.models import User
from apps.producto.models import Producto

# Create your models here.
class ItemCarrito(models.Model):
    cantidad = models.IntegerField(default=1)
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    producto = models.ForeignKey(Producto, on_delete=models.CASCADE)
    

class Carrito(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    items = models.ManyToManyField(ItemCarrito) 