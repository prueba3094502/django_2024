"""
URL configuration for tienda project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static 
from apps.home.views import home

#from apps.producto import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('producto/', include('apps.producto.urls')),
    path('carrito/', include('apps.carrito.urls')),
    path('venta/', include('apps.venta.urls')),
    path('usuario/', include('apps.usuario.urls')),
    path('home/', include('apps.home.urls')),

    path('', home, name='home')
    
]

urlpatterns += static( 'media/', document_root = settings.MEDIA_ROOT) 